export function getToken() {
    const token = localStorage.getItem('token');

    return token ? JSON.parse(token) : null;
}

export function getUser() {
    const user = localStorage.getItem('user');

    return user ? JSON.parse(user) : null;
}