require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './views/App';
import routes from './routes';
import store from './store/index'
import {auth} from './middleware/auth';

Vue.use(VueRouter);

// plugins
import { loadProgressBar } from 'axios-progress-bar';
import Multiselect from 'vue-multiselect';
import VeeValidate from 'vee-validate';
import VueCarousel from 'vue-carousel';
import Pagination from 'laravel-vue-pagination';

loadProgressBar();

Vue.component('multiselect', Multiselect);
Vue.component('pagination', Pagination);

Vue.use(VeeValidate);
Vue.use(VueCarousel);

const router = new VueRouter({
    mode: 'history',
    routes,
    linkActiveClass: 'active'
});

auth(store, router);

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {App},
});