import {getToken} from "../../auth";
import {getUser} from "../../auth";

const token = getToken();
const user = getUser();

const state = {
    currentToken: token,
    currentUser: user,
    isLoggedIn: false,
};

const getters = {
    currentUser(state) {
        return state.currentUser;
    },
    currentToken(state) {
        return state.currentToken;
    }
};

const actions = {

};

const mutations = {
    loginSuccess(state, payload) {
        state.isLoggedIn = true;
        state.currentToken = payload.data;
        localStorage.setItem('token', JSON.stringify(state.currentToken));
    },
    setCurrentUser(state, payload) {
        state.currentUser = payload.data;
        localStorage.setItem('user', JSON.stringify(state.currentUser));
    },
    loginFailed(state) {
        state.isLoggedIn = false;
    },
    logout(state) {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        state.currentToken = null;
        state.currentUser = null;
        state.isLoggedIn = false;
    },
};

export default {
    state,
    getters,
    actions,
    mutations
}