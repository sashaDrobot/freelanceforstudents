import Vue from "vue";
import Vuex from 'vuex'
import users from "./modules/users";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        title: 'App',
    },
    getters: {
        title(state) {
            return state.title;
        }
    },
    actions: {

    },
    mutations: {
        setTitle(state, payload) {
            state.title = payload;
        }
    },
    modules: {
        users,
    },
})