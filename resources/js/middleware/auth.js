export function auth(store, router) {
    router.beforeEach((to, from, next) => {
        const auth = to.matched.some(record => record.meta.auth);
        const token = store.getters.currentToken;

        if (auth && !token) {
            next('/login');
        } else if (!auth && token) {
            next('/');
        } else {
            next();
        }
    });

    axios.interceptors.response.use(null, (error) => {
        if(error.response.status === 401 || error.response.status === 500) {
            store.commit('logout');
            router.push('/login');
        }
    });
}