import App from './views/App';
import Login from './views/auth/Login';
import Register from './views/auth/Register';
import Home from './views/Home';
import EditUser from './views/EditUser';
import MyProjects from './views/MyProjects';
import Users from './views/Users';
import OpenUser from './views/OpenUser';
import Projects from './views/Projects';
import Error404 from './views/errors/Error404';

export default [
    {
        path: '/',
        name: 'app',
        component: App,
        meta: {
            auth: true
        },
        redirect: 'home',
        children: [
            {
                path: 'home',
                components: {
                    default: App,
                    content: Home
                },
                meta: {
                    auth: true
                }
            },
            {
                path: 'edit-profile',
                components: {
                    default: App,
                    content: EditUser
                },
                meta: {
                    auth: true
                }
            },
            {
                path: 'my-projects',
                components: {
                    default: App,
                    content: MyProjects
                },
                meta: {
                    auth: true
                }
            },
            {
                path: 'users',
                components: {
                    default: App,
                    content: Users
                },
                meta: {
                    auth: true
                }
            },
            {
                path: 'user/:id',
                components: {
                    default: App,
                    content: OpenUser
                },
                meta: {
                    auth: true
                }
            },
            {
                path: 'projects',
                components: {
                    default: App,
                    content: Projects
                },
                meta: {
                    auth: true
                }
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },
    {
        path: '*',
        component: Error404
    }
];