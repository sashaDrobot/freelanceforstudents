<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['name'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_skill');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project', 'project_skill');
    }
}
