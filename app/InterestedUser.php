<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestedUser extends Model
{
    protected $table = 'project_user';
}
