<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['title', 'description'];

    public function setParams()
    {
        $this->short_description = mb_strimwidth($this->description, 0, 250, ' ...');
        $this->skills = Project::find($this->id)->skills()->groupBy('name')->pluck('name');
        $this->files = json_decode($this->files);
        $this->posted_date = date('d F Y', strtotime($this->created_at));
        $owner = $this->user()->select('id', 'name', 'surname')->get()[0];
        $this->owner = [
            'id' => $owner['id'],
            'name' => $owner['name'] . ' ' . $owner['surname'],
            'link' => 'user/' . $owner['id']
        ];
        $this->interested_users = $this->users()->get()->unique();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps()->orderBy('updated_at');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill', 'project_skill');
    }
}
