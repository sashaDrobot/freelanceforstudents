<?php

namespace App\Http\Controllers;

use App\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    public function all()
    {
        $skills = Skill::groupBy('name')->pluck('name')->toArray();

        return response()->json($skills, 200);
    }
}
