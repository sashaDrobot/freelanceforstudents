<?php

namespace App\Http\Controllers;

use App\Project;
use App\Skill;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProjectController extends Controller
{
    public $userID;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->userID = auth('api')->user()->id;
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:200',
            'description' => 'required|string|max:10000',
            'skills' => 'sometimes|nullable|array',
            'skills.*' => 'sometimes|nullable|string|max:50',
            'project_type' => 'required|string|max:50',
            'number_of_students' => 'required|string|max:50',
            'project_length' => 'required|string|max:50',
            'files' => 'sometimes|nullable|array',
            'files.*' => 'sometimes|nullable|file|max:102400|mimetypes:application/*,image/*,text/*',
        ]);

        $project = new Project();
        $project->title = $request->title;
        $project->description = $request->description;
        $project->project_type = $request->project_type;
        $project->number_of_students = $request->number_of_students;
        $project->project_length = $request->project_length;

        if ($request->hasfile('files')) {
            $data = array();
            foreach ($request->file('files') as $file) {
                $name = 'project_' . $this->userID . '_' . time() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/upload/projects/', $name);
                $data[] = $name;
            }
            $project->files = json_encode($data);
        }

        $project->status = 'published';
        $project->user_id = $this->userID;
        $project->save();

        if ($request->filled('skills')) {
            $skills = $request->skills;
            $skillsId = [];

            foreach ($skills as $skill) {
                $item = Skill::firstOrNew(['name' => $skill]);
                $item->name = $skill;
                $item->save();
                $skillsId[] = $item->id;
            }
            $project->skills()->sync($skillsId);
            $project->save();
        }

        $projects = User::find($this->userID)->projects()->orderBy('created_at', 'desc')->get();

        foreach ($projects as $project) {
            $project->setParams();
        }

        return response()->json($projects, 200);
    }

    public function get(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:projects,id'
        ]);

        $project = Project::findOrFail($request->id);
        $project->setParams();

        return response()->json($project, 200);
    }

    public function my()
    {
        $projects = User::find($this->userID)->projects()->orderBy('created_at', 'desc')->get();

        foreach ($projects as $project) {
            $project->setParams();
        }

        return response()->json($projects, 200);
    }

    public function all()
    {
        $projects = Project::orderBy('created_at', 'desc')->paginate(10);

        foreach ($projects as $project) {
            $project->setParams();
        }

        return response()->json($projects, 200);
    }

    public function getProjectsByTag(Request $request)
    {
        $this->validate($request, [
            'tag' => 'sometimes|nullable|string|exists:skills,name'
        ]);

        $tag = $request->tag;

        if ($request->filled('tag')) {
            $projects = Skill::where('name', $tag)->firstOrFail()->projects()->orderBy('created_at', 'desc')->paginate(10);
        } else {
            $projects = Project::orderBy('created_at', 'desc')->paginate(10);
        }

        foreach ($projects as $project) {
            $project->setParams();
        }

        return response()->json($projects, 200);
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'title' => 'sometimes|nullable|string|max:200',
            'description' => 'sometimes|nullable|string|max:10000',
            'skills' => 'sometimes|nullable|array',
            'skills.*' => 'sometimes|nullable|string|max:50',
            'project_type' => 'sometimes|nullable|string|max:50',
            'number_of_students' => 'sometimes|nullable|string|max:50',
            'project_length' => 'sometimes|nullable|string|max:50',
        ]);

        $project = Project::find($request->id);

        if ($request->filled('title')) {
            $project->title = $request->title;
        }

        if ($request->filled('description')) {
            $project->description = $request->description;
        }
        if ($request->filled('project_type')) {
            $project->project_type = $request->project_type;
        }
        if ($request->filled('number_of_students')) {
            $project->number_of_students = $request->number_of_students;
        }
        if ($request->filled('project_length')) {
            $project->project_length = $request->project_length;
        }

        if ($request->hasfile('files')) {
            $data = array();
            foreach ($request->file('files') as $file) {
                $name = 'project_' . $this->userID . '_' . time() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/upload/projects/', $name);
                $data[] = $name;
            }
            $project->files = json_encode($data);
        }

        $project->save();

        if ($request->filled('skills')) {
            $skills = $request->skills;
            $skillsId = [];

            foreach ($skills as $skill) {
                $item = Skill::firstOrNew(['name' => $skill]);
                $item->name = $skill;
                $item->save();
                $skillsId[] = $item->id;
            }
            $project->skills()->sync($skillsId);
            $project->save();
        }

        $projects = User::find($this->userID)->projects()->orderBy('created_at', 'desc')->get();

        foreach ($projects as $project) {
            $project->setParams();
        }

        return response()->json($projects, 200);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:projects,id'
        ]);

        $user = User::findOrFail($this->userID);
        $project = Project::findOrFail($request->id);

        if ($user->can('delete', $project)) {
            $files = json_decode($project->files);

            if ($files) {
                foreach ($files as $file) {
                    File::delete('upload/projects/' . $file);
                }
            }

            $project->delete();
        }

        $projects = User::find($this->userID)->projects()->orderBy('created_at', 'desc')->get();

        foreach ($projects as $project) {
            $project->setParams();
        }

        return response()->json($projects, 200);
    }

    public function interested(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:projects,id'
        ]);

        $project = Project::findOrFail($request->id);
        $project->users()->attach($this->userID);
        $project->setParams();

        return response()->json($project, 200);
    }

    public function notInterested(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:projects,id'
        ]);

        $project = Project::findOrFail($request->id);
        $project->users()->detach($this->userID);
        $project->setParams();

        return response()->json($project, 200);
    }
}
