<?php

namespace App\Http\Controllers;

use App\Project;
use App\Skill;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public $userID;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->userID = auth('api')->user()->id;
    }

    public function update(Request $request)
    {
        $user = User::findOrFail($this->userID);

        $this->validate($request, [
            'name' => 'sometimes|nullable|string|max:255',
            'surname' => 'sometimes|nullable|string|max:255',
            'email' => [
                'sometimes',
                'nullable',
                'email',
                'string',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'phone' => 'sometimes|nullable|string|max:20',
            'position' => 'sometimes|nullable|string|max:200',
            'skills' => 'sometimes|nullable|array',
            'skills.*' => 'sometimes|nullable|string|max:50'
        ]);

        if ($request->filled('surname')) {
            $user->surname = $request->surname;
        }
        if ($request->filled('name')) {
            $user->name = $request->name;
        }
        if ($request->filled('email')) {
            $user->email = $request->email;
        }
        if ($request->has('phone')) {
            $user->phone = $request->phone;
        }
        if ($request->has('position')) {
            $user->position = $request->position;
        }
        if ($request->filled('skills')) {
            $skills = $request->skills;
            $skillsId = [];

            foreach ($skills as $skill) {
                $item = Skill::firstOrNew(['name' => $skill]);
                $item->name = $skill;
                $item->save();
                $skillsId[] = $item->id;
            }
            $user->skills()->sync($skillsId);
        }

        $user->save();

        $user->skills = $user->skills()->groupBy('name')->pluck('name');
        $user->portfolio = $user->portfolios()->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json($user, 200);
    }

    public function changePassword(Request $request)
    {
        $user = User::findOrFail($this->userID);

        $this->validate($request, [
            'old' => 'required|string|min:6',
            'new' => 'required|string|min:6'
        ]);

        if (Hash::check($request->old, $user->password)) {
            $user->password = Hash::make($request->new);
            $user->save();
        } else {
            return response()->json(['Invalid old password'], 422);
        }

        return response()->json(['Password successfully changed'], 200);
    }

    public function choosePhoto(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $user = User::findOrFail($this->userID);

        if ($user->avatar !== 'default.png') File::delete('upload/users/' . $user->avatar);

        $name = 'avatar_' . $this->userID . '_' . time() . '.' . $request->file('avatar')->getClientOriginalExtension();

        $path = public_path('upload/users/' . $name);

        Image::make($request->file('avatar'))->fit(600, 600)->save($path);

        $user->avatar = $name;
        $user->save();

        $user->skills = User::find($user->id)->skills()->groupBy('name')->pluck('name');
        $user->portfolio = $user->portfolios()->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json($user, 200);
    }

    public function destroy()
    {
        $user = User::findOrFail($this->userID);
        if ($user->avatar !== 'default.png') File::delete('upload/users/' . $user->avatar);
        // delete project file & portfolio
        $user->delete();

        auth('api')->logout();

        return response()->json(['User successfully deleted'], 200);
    }

    public function get(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:users,id'
        ]);

        $id = $request->id;

        $user = User::findOrFail($id);
        $user->skills = $user->skills()->groupBy('name')->pluck('name');

        if ($id == $this->userID) $user->portfolio = $user->portfolios()->orderBy('updated_at', 'desc')->get()->toArray();
        else $user->portfolio = $user->portfolios()->where('status', 'published')->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json($user, 200);
    }

    public function all()
    {
        $users = User::orderBy('created_at', 'desc')->paginate(10);

        foreach ($users as $user) {
            $user->skills = User::find($user->id)->skills()->groupBy('name')->pluck('name');
        }

        return response()->json($users, 200);
    }

    public function getUsersByTag(Request $request)
    {
        $this->validate($request, [
            'tag' => 'sometimes|nullable|string|exists:skills,name'
        ]);

        $tag = $request->tag;

        if ($request->filled('tag')) {
            $users = Skill::where('name', $tag)->firstOrFail()->users()->orderBy('created_at', 'desc')->paginate(10);
        } else {
            $users = User::orderBy('created_at', 'desc')->paginate(10);
        }

        foreach ($users as $user) {
            $user->skills = User::find($user->id)->skills()->groupBy('name')->pluck('name');
        }

        return response()->json($users, 200);
    }

    public function recommendedProjects()
    {
        $user = User::find($this->userID);

        $userSkills = $user->skills()->select('id')->pluck('id');

        $projectToSkills = [];

        $projects = Project::where('user_id', '!=', $this->userID)->get();
        foreach ($projects as $project) {
            $projectSkills = $project->skills()->select('id')->pluck('id')->toArray();
            $count = 0;
            foreach ($userSkills as $skill) {
                if (in_array($skill, $projectSkills)) {
                    $projectToSkills[$project->id] = ++$count;
                }
            }
        }

        arsort($projectToSkills);
		reset($projectToSkills);

        $projects = [];
        foreach ($projectToSkills as $projectId => $skillCount) {
            $project = Project::find($projectId);
            $project->setParams();
            $projects[] = $project;
        }

        return response()->json(collect($projects)->forPage(1, 8)->all());
    }
}
