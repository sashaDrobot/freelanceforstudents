<?php

namespace App\Http\Controllers;

use App\Portfolio;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class PortfolioController extends Controller
{
    public $userID;

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->userID = auth('api')->user()->id;
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:100',
            'description' => 'required|string|max:200',
            'url' => 'sometimes|nullable|url|max:100',
            'status' => 'required|boolean',
            'image' => 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'pdf' => 'sometimes|nullable|file|mimes:pdf'
        ]);

        $project = new Portfolio();
        $project->title = $request->title;
        $project->description = $request->description;
        $project->status = $request->status ? 'published' : 'saved';

        if ($request->file('image')) {
            $name = 'project_' . $this->userID . '_' . time() . '.' . $request->file('image')->getClientOriginalExtension();
            $path = public_path('upload/portfolio/' . $name);
            Image::make($request->file('image'))->fit(900, 540)->save($path);
            $project->image = $name;
        }

        if ($request->file('pdf')) {
            $name = 'pdf_' . $this->userID . '_' . time() . '.' . $request->file('pdf')->getClientOriginalExtension();
            $request->file('pdf')->move(public_path() . '/upload/portfolio/', $name);
            $project->link = '/upload/portfolio/' . $name;
            $project->link_type = 'pdf';
        } elseif ($request->filled('url')) {
            $project->link = $request->url;
            $project->link_type = 'website';
        }

        $project->user_id = $this->userID;

        $project->save();

        $user = User::findOrFail($this->userID);

        $user->skills = $user->skills()->groupBy('name')->pluck('name');
        $user->portfolio = $user->portfolios()->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json($user, 200);
    }

    public function get(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:portfolios,id'
        ]);

        $user = User::findOrFail($this->userID);
        $project = Portfolio::findOrFail($request->id);

        if ($user->can('get', $project)) {
            return response()->json($project, 200);
        }

        return response()->json('No access', 403);
    }

    public function edit(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:portfolios,id',
            'title' => 'sometimes|nullable|string|max:100',
            'description' => 'sometimes|nullable|string|max:200',
            'url' => 'sometimes|nullable|url|max:100',
            'status' => 'sometimes|nullable|boolean',
            'image' => 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'pdf' => 'sometimes|nullable|file|mimes:pdf',
            'delete_photo' => 'sometimes|nullable|boolean',
            'delete_url' => 'sometimes|nullable|boolean',
        ]);

        $user = User::findOrFail($this->userID);
        $project = Portfolio::findOrFail($request->id);

        if ($user->can('update', $project)) {

            $project->title = $request->title;
            $project->description = $request->description;
            $project->status = $request->status ? 'published' : 'saved';

            if ($request->file('image')) {
                if ($project->image !== 'portfolio_default.jpg') File::delete('upload/portfolio/' . $project->image); // Storage::delete('public/upload/portfolio/' . $project->image);
                $name = 'project_' . $this->userID . '_' . time() . '.' . $request->file('image')->getClientOriginalExtension();
                $path = public_path('upload/portfolio/' . $name);
                Image::make($request->file('image'))->fit(900, 540)->save($path);
                $project->image = $name;
            } elseif ($request->delete_photo) {
                if ($project->image !== 'portfolio_default.jpg') File::delete('upload/portfolio/' . $project->image); // Storage::delete('public/upload/portfolio/' . $project->image);
                $project->image = 'portfolio_default.jpg';
            }

            if ($request->filled('url')) {
                if ($project->link_type == 'pdf') unlink(public_path($project->link));
                $project->link = $request->url;
                $project->link_type = 'website';
            } elseif ($request->file('pdf')) {
                if ($project->link_type == 'pdf') unlink(public_path($project->link));
                $name = 'pdf_' . $this->userID . '_' . time() . '.' . $request->file('pdf')->getClientOriginalExtension();
                $request->file('pdf')->move(public_path() . '/upload/portfolio/', $name);
                $project->link = '/upload/portfolio/' . $name;
                $project->link_type = 'pdf';
            } elseif ($request->delete_pdf) {
                if ($project->link_type == 'pdf') unlink(public_path($project->link));
                $project->link = '';
                $project->link_type = '';
            }

            $project->save();
        }

        $user->skills = $user->skills()->groupBy('name')->pluck('name');
        $user->portfolio = $user->portfolios()->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json($user, 200);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer|exists:portfolios,id'
        ]);

        $user = User::findOrFail($this->userID);
        $project = Portfolio::findOrFail($request->id);

        if ($user->can('delete', $project)) {
            if ($project->image !== 'portfolio_default.jpg') File::delete('upload/portfolio/' . $project->image);
            if ($project->link_type == 'pdf') unlink(public_path($project->link));
            $project->delete();
        }

        $user->skills = $user->skills()->groupBy('name')->pluck('name');
        $user->portfolio = $user->portfolios()->orderBy('updated_at', 'desc')->get()->toArray();

        return response()->json($user, 200);
    }
}
