<?php

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::put('update-profile', 'UserController@update');
    Route::put('change-password', 'UserController@changePassword');
    Route::post('choose-photo', 'UserController@choosePhoto');
    Route::post('delete-user', 'UserController@destroy');
    Route::get('get-skill-list', 'SkillController@all');

    Route::get('get-recommended-projects', 'UserController@recommendedProjects');

    Route::post('add-project-to-portfolio', 'PortfolioController@add');
    Route::post('get-project-for-edit', 'PortfolioController@get');
    Route::post('edit-project-in-portfolio', 'PortfolioController@edit');
    Route::delete('delete-project-from-portfolio', 'PortfolioController@delete');

    Route::post('to-be-interested', 'ProjectController@interested');
    Route::post('not-interested', 'ProjectController@notInterested');
    Route::post('add-project', 'ProjectController@add');
    Route::post('get-project', 'ProjectController@get');
    Route::get('get-my-projects', 'ProjectController@my');
    Route::get('get-projects-list', 'ProjectController@all');
    Route::post('get-projects-by-tag', 'ProjectController@getProjectsByTag');
    Route::post('edit-project', 'ProjectController@edit');
    Route::delete('delete-project', 'ProjectController@delete');

    Route::get('get-users-list', 'UserController@all');
    Route::post('get-users-by-tag', 'UserController@getUsersByTag');
    Route::post('get-user', 'UserController@get');
});