<?php

use Illuminate\Http\Request;

Route::get('/{path}', 'AppController@index')->where('path', '(.*)');