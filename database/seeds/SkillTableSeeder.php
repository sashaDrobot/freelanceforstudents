<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert([
            ['name' => 'PHP'],
            ['name' => 'Laravel'],
            ['name' => 'Symfony'],
            ['name' => 'Docker'],
            ['name' => 'Vue'],
            ['name' => 'MySQL'],
            ['name' => 'HTML'],
            ['name' => 'CSS'],
            ['name' => 'JS'],
            ['name' => 'C++'],
            ['name' => 'React'],
            ['name' => 'jQuery'],
            ['name' => 'Nodejs'],
            ['name' => 'MongoDB'],
            ['name' => 'QA'],
        ]);
    }
}
